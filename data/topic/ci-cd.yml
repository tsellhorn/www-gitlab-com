title: Continuous integration
seo_title: Continuous integration (CI)
description: Learn how continuous integration automates software development
  workflows and improves code quality.
header_body: >-
  Continuous integration automates software development workflows, reduces
  costs, and ensures code quality.



  [Watch a GitLab CI/CD webcast →](https://about.gitlab.com/webcast/mastering-ci-cd/)
canonical_path: /topics/ci-cd/
file_name: ci-cd
twitter_image: /images/opengraph/ci-cd-opengraph.png
related_content:
  - title: Continuous integration pipelines
    url: /topics/ci-cd/continuous-integration-pipeline/
  - title: Benefits of continuous integration
    url: /topics/ci-cd/benefits-continuous-integration/
  - title: Implement continuous integration
    url: /topics/ci-cd/implement-continuous-integration/
  - title: Continuous integration best practices
    url: /topics/ci-cd/continuous-integration-best-practices/
  - title: Pipeline as code
    url: /topics/ci-cd/pipeline-as-code/
  - title: Shift left DevOps
    url: /topics/ci-cd/shift-left-devops/
  - title: How to choose the right continuous integration tool
    url: /topics/ci-cd/choose-continuous-integration-tool/
  - title: What is a continuous integration server?
    url: /topics/ci-cd/continuous-integration-server/
  - title: What is cloud native continuous integration?
    url: /topics/ci-cd/cloud-native-continuous-integration/
  - title: Continuous integration in Agile development
    url: /topics/ci-cd/continuous-integration-agile/
  - title: Continuous integration metrics
    url: /topics/ci-cd/continuous-integration-metrics/
cover_image: /images/topics/g_gitlab-ci-cd.svg
body: >-
  [Continuous integration (CI)](/stages-devops-lifecycle/continuous-integration/) enables DevOps teams

  to increase the speed of software development through automation. Continuous integration works to implement code from your team in a

  shared repository which vastly improves your deployment pipeline. Developers that

  employ use CI share their new code in a merge (pull) request, which

  triggers a pipeline to build, test, and validate the new code before merging

  the changes in the repository. 


  All code is tested throughout each stage to secure better quality builds and applications with fewer bugs. CI/CD pipelines can determine what happens when builds pass or fail these tests, meaning that errors are identified much faster. As code goes through each stage of the development process, it is continually validated against many other changes in the repository happening concurrently, which ensures code integrity throughout the pipeline. Together, CI and CD accelerate how quickly your team delivers results for your customers and stakeholders.


  > **Continuous Integration** is the practice of integrating code into a shared repository and building/testing each change automatically, as early as possible; usually several times a day.



  ## What are the benefits of continuous integration?


  1. More time for innovation

  2. Better retention rates

  3. More revenue

  4. Business efficiency


  [Learn more about the benefits of continuous integration →](/topics/ci-cd/benefits-continuous-integration/)
benefits_title: CI fundamentals
benefits_description: Continuous integration is all about efficiency and is
  built around these core elements to make it effective.
benefits:
  - title: A single source repository
    description: Source code management (SCM) that houses all necessary files and
      scripts to create builds.
    image: /images/icons/first-look-influence.svg
  - title: Automated builds
    description: Scripts should include everything you need to build from a single command.
    image: /images/icons/build.svg
  - title: Builds should be self-testing
    description: Testing scripts should ensure that the failure of a test should
      result in a failed build.
    image: /images/icons/computer-test.svg
  - title: Frequent iterations
    description: Multiple commits to the repository mean there are fewer places for
      conflicts to hide.
    image: /images/icons/scale.svg
  - title: Stable testing environments
    description: Code should be tested in a cloned version of the production environment.
    image: /images/icons/stable-computer.svg
  - title: Maximum visibility
    description: Every developer should be able to access the latest executables and
      see any changes made to the repository.
    image: /images/icons/visibility.svg
cta_banner:
  - title: Why Gitlab CI/CD?
    body: >-
      In order to complete all the required fundamentals of full CI/CD, many CI
      platforms rely on integrations with other tools to fulfill those needs.
      Many organizations have to maintain costly and complicated toolchains in
      order to have full CI/CD capabilities. This often means maintaining a
      separate SCM like Bitbucket or GitHub, connecting to a separate testing
      tool, that connects to their CI tool, that connects to a deployment tool
      like Chef or Puppet, that also connects to various security and monitoring
      tools.


      Instead of just focusing on building great software, organizations have to also maintain and manage a complicated toolchain. GitLab is a single application for the entire DevOps lifecycle, meaning we fulfill all the fundamentals for CI/CD in one environment.
  - title: GitLab CI/CD rated
    subtitle: Build, test, deploy, and monitor your code from a single application.
    body: We believe a single application that offers visibility across the entire
      SDLC is the best way to ensure that every development stage is included
      and optimized. When everything is under one roof, it’s easy to pinpoint
      workflow bottlenecks and evaluate the impact each element has on
      deployment speed. GitLab has CI/CD built right in, no plugins required.
    cta:
      - url: /stages-devops-lifecycle/continuous-integration/
        text: Explore GitLab CI
      - url: /stages-devops-lifecycle/continuous-delivery/
        text: Explore GitLab CD
resources_title: CI Resources
resources_intro: >
  Here’s a list of resources on CI that we find to be particularly helpful. We
  would love to get your recommendations on books, blogs, videos, podcasts, and
  other resources that tell a great story or offer valuable insight.


  Share your favorites with us by tweeting us [@gitlab](https://twitter.com/gitlab)!
resources:
  - title: Mastering continuous software development
    url: /webcast/mastering-ci-cd/
    type: Webcast
  - title: Scaled continuous integration and delivery
    url: /resources/scaled-ci-cd/
    type: Whitepapers
  - title: The benefits of single application CI/CD
    url: /resources/ebook-single-app-cicd/
    type: Whitepapers
  - title: Cloud Native Computing Foundation (CNCF)
    url: /customers/cncf/
    type: Case studies
  - title: Verizon
    url: /blog/2019/02/14/verizon-customer-story/
    type: Case studies
  - title: Ticketmaster
    url: /blog/2017/06/07/continous-integration-ticketmaster/
    type: Case studies
  - title: Voted a leader in The 2019 Forrester Wave Cloud-Native Continuous
      Integration Tools
    url: /resources/forrester-wave-cloudnative-ci/
    type: Reports
  - title: Voted as a Strong Performer in The Forrester Wave™ Continuous Delivery
      And Release Automation, Q2 2020
    url: /analysts/forrester-cdra20/
    type: Reports
suggested_content:
  - url: /blog/2019/04/02/why-gitlab-ci-cd/
  - url: /blog/2018/01/22/a-beginners-guide-to-continuous-integration/
  - url: /blog/2019/04/25/5-teams-that-made-the-switch-to-gitlab-ci-cd/
  - url: /blog/2019/06/27/positive-outcomes-ci-cd/
  - url: /blog/2019/06/21/business-impact-ci-cd/
  - url: /blog/2018/08/09/how-devops-and-gitlab-cicd-enhance-a-frontend-workflow/
schema_faq:
  - question: What is CI/CD
    answer: Continuous integration (CI) and continuous delivery (CD) enable DevOps
      teams to increase the speed of software development and deliver better
      quality code, faster. Continuous integration works to integrate code from
      your team in a shared repository vastly improving your deployment
      pipeline. Developers share their new code in a Merge (Pull) Request, which
      triggers a pipeline to build, test, and validate the new code before
      merging the changes in your repository. Continuous delivery deploys
      CI-validated code to your application.
    cta:
      - text: Learn more about CI/CD
        url: https://about.gitlab.com/topics/ci-cd/
  - question: Benefits of CI/CD
    answer: >-
      CI/CD automates workflows and reduces error rates within a production
      environment, which can have far-reaching impacts on not just development
      teams but throughout a whole organization.


      * More time for innovation

      * Better retention rates

      * More revenue

      * Business efficiency
    cta:
      - url: https://about.gitlab.com/topics/ci-cd/
        text: Learn more about the benefits of CI/CD
  - question: Why Gitlab CI/CD?
    answer: GitLab is a single application for the entire DevOps lifecycle, meaning
      we fulfill all the fundamentals for CI/CD in one environment.
    cta:
      - url: https://about.gitlab.com/topics/ci-cd/#why-gitlab-ci-cd
        text: Learn more about CI/CD
body_title: What is CI/CD?
