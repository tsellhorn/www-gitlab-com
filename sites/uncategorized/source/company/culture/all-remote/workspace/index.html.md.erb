---
layout: handbook-page-toc
title: "Considerations for a Productive Home Office or Remote Workspace"
canonical_path: "/company/culture/all-remote/workspace/"
description: Considerations for a Productive Home Office or Remote Workspace
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

![GitLab code illustration](/images/all-remote/gitlab-code-review.jpg){: .medium.center}

On this page, we're detailing a variety of considerations and recommendations for constructing an ergonomic, productive, and fulfilling home office or remote workplace. 

## How do I design and build the optimal home office?

<blockquote class="twitter-tweet tw-align-center"><p lang="en" dir="ltr">Now that everyone works from home the standard for video setups will go up a lot<br><br>Want to stand out in a call? Have a good camera &amp; mic.<br>Instantly makes you look 10x professional<br><br>Compare <a href="https://twitter.com/Jobvo?ref_src=twsrc%5Etfw">@Jobvo</a> to me here.<br>Guess who the successful CEO of a top remote startup is and who a clown <a href="https://t.co/ixorYPM8tD">pic.twitter.com/ixorYPM8tD</a></p>&mdash; Andreas Klinger ✌️ (@andreasklinger) <a href="https://twitter.com/andreasklinger/status/1238325463300202497?ref_src=twsrc%5Etfw">March 13, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Particularly for those who are working remotely for the first time, the notion of [constructing a home office](/blog/2019/09/12/not-everyone-has-a-home-office/) can be a perplexing one. In colocated settings, environmental design professionals are retained to create the work setting. In a remote environment, that burden shifts to the employee. 

While there is tremendous [benefit](/company/culture/all-remote/benefits/) to ditching the cubicle, it's not uncommon for a remote worker to feel ill-equipped to design their own workspace. A bespoke work area is fundamentally different than any other area in the home. It's important to be intentional about the space where you do the bulk of your work, designing it to be comfortable and to create an atmosphere where you are productive and focused. 

It's important to view your home office as an evolving project. [Iteration](/handbook/values/#iteration) applies to workspaces, too. As you begin to interact with your office, you may determine that monitors should be shifted, lighting should be added or tweaked, or that you actually prefer silence or ambient noise over headphones playing music. 

For a glimpse at how one GitLab team member thought through his home workspace, check out Brandon L.'s blog post, *[How To Stay Productive In Your Home Office](/blog/2019/11/06/how-to-stay-productive-in-your-home-office/)*.

### Examples of work-from-home setups

- GitLab Head of Remote, Darren M., shares [details on his work-from-home arrangement](/handbook/marketing/readmes/dmurph/#work-from-home-office-setup), which includes a mirrorless camera as a webcam for optimal video quality.
- GitLab Chief Technology Officer, Eric Johnson, [shares the details of his set up in his README](/handbook/engineering/readmes/eric-johnson/#my-remote-work-setup).
- Job van der Voort, CEO at Remote, provides [a breakdown of his top-notch home office setup](https://blog.remote.com/maximum-productivity-home-office){:target="_blank"}. 
- GitLab Developer Evangelist Michael F. [details his current setup](https://dnsmichi.at/all-remote-workspace/){:target="_blank"} with helpful tips on lighting, apps and settings, and how to personalize your workspace. 
- Emma Bostian, Software Engineer at Spotify, [writes about her home-office tech setup for recording courses and podcasts](https://compiled.blog/blog/my-tech-setup){:target="_blank"} 
- Andreas Klinger, founder at [Remote First Capital](https://www.remotefirstcapital.com/){:target="_blank"}, maintains an [ongoing Twitter thread](https://twitter.com/andreasklinger/status/1238325463300202497){:target="_blank"} of exceptional at-home workspaces. 

## Not sure what to buy?

Look at our [equipment examples](https://about.gitlab.com/handbook/finance/expenses/#-not-sure-what-to-buy) to see some of the items that other GitLab team members have purchased, and please consider adding to the list if there is something you'd like to share.

## Chairs

Seating should be viewed not as an expense, but as an *investment* — in your health, comfort, and productivity. 

While each person's primary workspace will vary, it's important to place a high degree of consideration on your chair. Many remote workers spend a significant portion of their day seated. While [resources exist](https://www.steelcase.com/research/articles/topics/ergonomics/movement-in-the-workplace/){:target="_blank"} to explain how frequently you should take a break, stand up, stretch, and engage in activity during the workday, you should be careful not to skimp on seating. 

Unless you plan to utilize a standing desk, which is covered below, your chair is apt to be the single most important element of your home office. Not all ergonomic chairs are created equal, and a chair that works well for one person may not be ideal for another. It's important to [consider your posture](https://my.clevelandclinic.org/health/articles/4485-back-health-and-posture){:target="_blank"}, and make adjustments to habits if needed, to make the most of an ergonomic chair. 

If at all possible, visit a physical store to try out a variety of ergonomic seating options, or purchase online from a retailer that offers a generous return policy. 

[Steelcase](https://www.steelcase.com/products/office-chairs/){:target="_blank"} and [Herman Miller](https://www.hermanmiller.com/products/seating/){:target="_blank"} both offer solid options. While retail pricing on chairs from these firms are lofty, they are commonly found on used marketplaces for less. To assist in your research, consider [Wirecutter's exhaustive (and continually updated) guide to office chairs](https://thewirecutter.com/reviews/best-office-chair/){:target="_blank"}. 

For employers, consider offering an [allowance](/handbook/spending-company-money/) for employees to invest in a high-quality ergonomic chair.

## Desks

While some remote workers prefer to [bounce around](/blog/2019/09/23/how-to-push-code-from-a-hammock/) during the day, those looking to [invest in a desk](https://www.nationalbusinessfurniture.com/blog/complete-guide-to-office-desks){:target="_blank"} for a dedicated workspace should consider a few elements. 

It's wise to plan your desk purchase around your equipment list, rather than the other way around. If you plan to use one or more desktop monitors (which is recommended), measure the physical dimensions and ensure that your desk has room to hold them. Factor in other items such as external speakers, scanners, printers, microphone mounts, webcams, external webcam lighting, etc. 

Generally, a larger desk is preferred, as it reduces crowding. A clean, uncrowded desk is a happier place to work from. 

### Standing desks

<blockquote class="twitter-tweet tw-align-center"><p lang="en" dir="ltr">new desk setup <a href="https://t.co/AqPUPA8fTm">pic.twitter.com/AqPUPA8fTm</a></p>&mdash; jake burden (@jake_burden_) <a href="https://twitter.com/jake_burden_/status/1333471036474597377?ref_src=twsrc%5Etfw">November 30, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Should your budget allow, [an adjustable standing desk](https://www.autonomous.ai){:target="_blank"} is a great, ergonomic option. This allows you to easily move your desk higher in order to stand while working, and then adjust it back down to sit for a time. This is a good article on [The Best Standing Desks](https://thewirecutter.com/reviews/best-standing-desk/){:target="_blank"} in higher price ranges.

GitLab team members have been satisfied with the value and quality of standing desks from [Autonomous](https://www.autonomous.ai/){:target="_blank"} and [Fully](https://www.fully.com/standing-desks.html){:target="_blank"}. 

Research is ongoing as to the [ideal ratio of sitting to standing](https://uwaterloo.ca/kinesiology/how-long-should-you-stand-rather-sit-your-work-station){:target="_blank"} during a workday. 

> Using advanced ergonomic and health risk calculations, [Jack Callaghan](https://uwaterloo.ca/kinesiology/people-profiles/jack-callaghan){:target="_blank"}, a professor in Waterloo’s Department of Kinesiology, has found that the ideal sit-stand ratio lies somewhere between 1:1 and 1:3  – a vast departure from traditional wisdom. 

### Standing mats

If you plan to stand for an extended portion of your working day, consider investing in a [standing desk mat](https://thewirecutter.com/reviews/best-standing-desk-mat/){:target="_blank"}. Also called anti-fatigue mats, these provide cushion for one's feet and enables a more natural shifting of weight while working. 

## Webcams and mirrorless cameras

In an all-remote setting, [video calls](/handbook/communication/#video-calls) are vital to maintaining close relationships with clients, partners, and colleagues. While voice calls are flexible and allow for uniquely efficient lifestyles (e.g. listening to a conference call while running in a park), it's important to integrate video into [workplace communication](/company/culture/all-remote/meetings/). 

There are generally three tiers of camera quality for at-home workspaces (professional studio rigs notwithstanding).

1. Mirrorless camera or DSLR with dedicated lens (high-end)
1. Dedicated USB webcam (mid-tier)
1. Inbuilt webcam on a phone, laptop, or external display (baseline)

### Mirrorless cameras are the new webcams

<blockquote class="twitter-tweet tw-align-center"><p lang="en" dir="ltr">megadesk back in action. i dont think i can be a single monitor person though<br><br>whats best? multiple, single, or ultrawide <a href="https://t.co/yRjlzx5lBz">pic.twitter.com/yRjlzx5lBz</a></p>&mdash; Chris Gets Better (@chrisgetsbetter) <a href="https://twitter.com/chrisgetsbetter/status/1243620518281928705?ref_src=twsrc%5Etfw">March 27, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

For those seeking optimal image quality in video calls, a dedicated mirrorless camera or DSLR is recommended. *The Verge* maintains a [basic setup guide](https://www.theverge.com/21244380/webcam-camera-how-to-dslr-mirrorless-capture-card-usb-hdmi){:target="_blank"} if you're new to the concept. Sony's Alpha line of mirrorless cameras are excellent for webcam use. Before committing, read the following considerations.

1. Make sure your camera supports constant AC power (typically via micro-USB). If not, order a dummy adapter which slots in your battery compartment. 
1. Determine if you need a USB capture card, which converts a camera's HDMI video output stream into a Zoom-friendly webcam stream. Many manufacturers, like Sony and Nikon, are [releasing software](https://imagingedge.sony.net/en-us/ie-desktop.html){:target="_blank"} that negates the need for this added piece of hardware. If you *do* need a capture card, Elgato's [Cam Link](https://www.elgato.com/en/gaming/cam-link-4k){:target="_blank"} or [HD60 S+](https://www.elgato.com/en/gaming/game-capture-hd60-s-plus){:target="_blank"} are solid options, as is the [IOGEAR HDMI to USB Type-C Video Capture Adapter](https://www.bhphotovideo.com/c/product/1455743-REG/iogear_guv301_hdmi_to_usb_c_video.html){:target="_blank"}. Linux users: be aware that Elgato's capture cards [aren't supported](https://twitter.com/sytses/status/1329206645855981569){:target="_blank"}, so you'll want to consider an alternative from [ClonerAlliance](https://www.amazon.com/ClonerAlliance-Passthrough-Capture-Ultra-Low-Consoles/dp/B07YY52YP6){:target="_blank"}. 
1. Toggle the right settings. Put your camera in movie mode, enable clean HDMI output (so you don't have menu icons in your webcam feed), disable Auto Power Off, enable servo tracking (if applicable), enable continuous face tracking (if available), and tweak the exposure and white balance to create a normalized image based on your room's lighting conditions. 
1. Select a camera lens that stops down to f/1.8. The ideal amount of bokeh is generally found between f/2.0 and f/2.8. Experiment with your aperture settings to find a depth of field that you're comfortable with. A prime lens is ideal, as you don't have to reconfigure the zoom each time you power your camera on. However, a zoomable lens provides more flexibility if you enjoy wide shots for certain calls and tight shots for interviews. 
1. Don't skimp on a mounting solution. You'll need a heavy duty desk clamp, such as the [Manfrotto 035RL](https://www.bhphotovideo.com/c/product/546356-REG/Manfrotto_035RL_035RL_Super_Clamp_with.html){:target="_blank"}, and articulating arm like the [Impact 3 Section](https://www.bhphotovideo.com/c/product/1015864-REG/impact_3_section_double_articulated.html){:target="_blank"} to manage the heft of a mirrorless camera or DSLR with lens attached. 

### Making the most of a webcam

While most phones and laptops ship with passable webcams, they do not offer optimal quality. A dedicated webcam like the [Logitech Brio Ultra HD Pro](https://www.logitech.com/en-us/product/brio?crid=34){:target="_blank"} offers a higher resolution camera compared to inbuilt cameras, and is able to handle low-light scenarios with greater poise. Many dedicated webcams also include a software suite for touching up one's appearance, tweaking white balance, and applying background themes when paired with a green screen. These are also less complicated to install and maintain compared with mirrorless/DSLR rigs. 

Consider [selecting a webcam](https://thewirecutter.com/reviews/the-best-webcams/){:target="_blank"} with a versatile mount, enabling it to be set atop a desktop monitor as well as a laptop. You can greatly improve the output from a dedicated webcam by focusing on [good lighting](https://www.nytimes.com/wirecutter/blog/video-call-lighting-tips/){:target="_blank"}. By utilizing a ring light or [Elgato Key Light](https://www.elgato.com/en/gaming/key-light){:target="_blank"}, and ensuring that there are no windows to your side or rear, you'll create a more even picture with fewer shadows. For more, read [5 tips for mastering video calls](/blog/2019/08/05/tips-for-mastering-video-calls/) on the GitLab blog. 

## Headphones

Particularly in noisy environments, wearing headphones creates [a more positive video experience for all](/blog/2019/06/28/five-things-you-hear-from-gitlab-ceo/). Your choice of headphone will vary depending on your workspace. For example, if you're using a dedicated microphone, you may prefer comfortable studio-style headphones without an in-line mic. If you want to reduce the amount of hardware you're using, headphones with an in-line mic will be more appropriate. 

GitLab recommends trying out various headphone styles in advance if possible. Open-ear vs. closed-ear, for example, provides a very different listening experience. Noise-cancelling headphones are great for crowded coworking spaces, but may blot out too much sound for [stay-at-home parents](/company/culture/all-remote/people/#parents) who need to be aware of what's happening inside the home. 

Some prefer in-ear headphones rather than over-the-head headphones, and it's important to consider long-term comfortability for those who may find themselves in [video calls](/blog/2019/08/05/tips-for-mastering-video-calls/) for multiple hours per day. 

Bluetooth headphones can be problematic for roles requiring a significant amount of calls due to limitations of microphone quality, latency, and battery life. If you opt for Bluetooth headphones, it is recommended that you use a separate wired microphone. Too, be mindful of Bluetooth headphones being paired with multiple devices (e.g. one pair of headphones with a pairing history involving a laptop as well as a phone). Bluetooth headsets can easily jump between devices, thus it is recommended to only pair one set per device to avoid unexpected disconnects during video calls. 

What constitutes "good headphones" varies significantly depending on preference. We recommend digging into [Wirecutter's various headphone guides](https://thewirecutter.com/electronics/headphones/){:target="_blank"} for researched suggestions.

## Lighting

In colocated settings, the human eye is capable of focusing on conversation participants while deprioritizing suboptimal surroundings. When [communicating](/company/culture/all-remote/informal-communication/) via webcam, one needs to be more cognizant of the lighting around them.

[Meetings](/company/culture/all-remote/meetings/) are about the work, [not the background](/company/culture/all-remote/meetings/#meetings-are-about-the-work-not-the-background), but those who are [designing their home office](/blog/2019/09/12/not-everyone-has-a-home-office/) may want to consider lighting before too many absolutes are put in place. Here are a few lighting tips to be mindful of.

1. Avoid backlighting or sidelighting when possible (e.g. design your office so that outside light shines onto your face, not your back or side).
1. Consider smart bulbs (e.g. [Philips Hue](https://www2.meethue.com/en-us/bulbs){:target="_blank"}) to light your office, which can be tweaked to create a soft, pleasing light regardless of your wall color. 
1. While [enclosed rooms](/blog/2019/09/12/not-everyone-has-a-home-office/) are ideal for controlling light, they may feel inhibiting to work from.
1. Aim to avoid shadows and changing light conditions.

While studio lighting is ideal, not everyone will be inclined to install large, heat-generating light boxes in their home office. As remote work and livestreaming become more popular, companies are devising smaller solutions. Elgato's [Key Light](https://www.youtube.com/watch?v=d2qR-wMPoTE){:target="_blank"} is a great example. By placing one Key Light at the edge of a desk and facing its LEDs directly into the wall, a soft, refreshing light is [bounced back onto the participant](https://www.youtube.com/watch?v=RckLFNRKPfU){:target="_blank"}.

[DIY solutions](https://www.diyphotography.net/look-good-webcam-vlog/){:target="_blank"} are relatively easy to create with a mount and a light ring.

## Microphones

"_When audio quality is high (vs low), people judge the content as better and more important. They also judge the speaker as more intelligent, competent, and likable. Messages that are difficult to process are less compelling._" This [research](https://tips.ariyh.com/p/good-sound-quality-smarter), conducted by Norbert Schwarz at the University of Southern California, reinforces the importance of investing in equipment and atmosphere to generate the highest-fidelity audio in a remote work setting.

Whenever possible, avoid using the inbuilt microphone of a laptop, phone, or desktop monitor when communicating. These microphones tend to be of low quality, and do little to stop background noise. 

At the very least, utilize a pair of Bluetooth or wired earbuds with an in-line microphone. These are commonly included with most smartphones. 

For those who spend significant time on video calls, consider a [dedicated USB microphone](https://thewirecutter.com/reviews/the-best-usb-microphone/){:target="_blank"} (and, if desired, a desk mount for added ergonomic positioning). For example, [Blue Microphones](https://www.bluedesigns.com/){:target="_blank"} offers a variety of options that are crafted with creators, streamers, and podcasters in mind, and all provide exceptional clarity and noise reduction on [video calls](/handbook/communication/#video-calls). 

Several GitLab team members have positive experiences with the [M-Audio UberMic](https://www.amazon.com/M-Audio-Uber-Mic-Professional-Microphone/dp/B0767N58ZY){:target="_blank"}. 

## Monitors

If you feel that your digital workspace is too cramped when relying solely on a laptop, consider using at least one [external monitor](https://thewirecutter.com/reviews/best-monitors/){:target="_blank"}. To boot, external monitors offer flexible positioning, which allows your neck to be situated in a more natural position. 

External monitors are especially useful to remote employees, as much of their day is made up of video calls. An external display enables one screen to be used for video chatting, while another screen is used for documentation, referencing pages, etc. 

If you feel overwhelmed by too much information spread across multiple monitors, consider [window management software solutions](https://thesweetsetup.com/window-management-macos-2018/){:target="_blank"} such as [Divvy](https://mizage.com/divvy/){:target="_blank"} and [Magnet](https://apps.apple.com/us/app/magnet/id441258766?ign-mpt=uo%3D4&mt=12){:target="_blank"}. 

If you'd like a portable monitor and you primarily use a MacBook laptop, consider expanding your visual workspace with [Sidecar](https://www.apple.com/newsroom/2019/06/apple-previews-macos-catalina/){:target="_blank"} (available in macOS Catalina). This allows an iPad to double as a secondary display for your Mac.

## Teleprompter

If you're planning to present on a regular basis, consider a teleprompter. To create training modules or asynchronous video overviews, a teleprompter enables you to read a pre-written script while looking directly into a lens. This creates a more authentic connection with the audience, akin to a newscaster on television. 

For a basic solution, consider the [Padcaster Parrot Smartphone Teleprompter](https://padcaster.com/pages/parrot){:target="_blank"}. If you have a smartphone and mirrorless/DSLR camera already, this $99 kit provides everything else you need.

Matt Mullenweg (CEO, Automattic) published an exhaustive overview of his [streaming setup at home](https://ma.tt/2020/05/ceo-video-streaming/){:target="_blank"}, including an [Ikan Elite](https://www.amazon.com/dp/B019H3B2DE/){:target="_blank"} teleprompter rig. 

Sid Sijbrandij (CEO and co-founder, GitLab) shared a video overview of his [streaming and teleprompter setup on GitLab Unfiltered](https://youtu.be/or86AXuXgYI){:target="_blank"}. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/or86AXuXgYI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*GitLab CEO Teleprompter Setup; captured during [GitLab Commit Virtual 2020](/events/commit/)*

### Limitations for Everyday Video Conferencing

Teleprompters enable eye contact similar to meeting someone in-person and can make meetings more engaging. However, there are currently challenges with using a teleprompter setup as your everyday video conferencing setup. Once these are overcome we may recommend it.

1. **Problem (Major):** When Zoom is on the teleprompter the chat, controls, and screen sharing are extremely small and unusable. They have a “dual monitor” mode that is meant for screen sharing and it behaves as expected for n=3+ calls. But for 1:1 calls it unexpectedly puts yourself on the video window in the teleprompter.
    * *Mitigation:* We may need to wait for Zoom to roll out a new mode that is teleprompter-specific
1. **Problem (Major):** The mouse, Zoom controls, and screen shared content are horizontally flipped on the teleprompter making it very difficult to work
    * *Mitigation:* Buy [a device](https://telepromptermirror.com/hdmi-mirrorbox/){:target="_blank"} that horizontally flips the HDMI output to the monitor mounted in the teleprompter
1. **Problem (Minor):** It's disconcerting that the audio of the person you're looking at is coming from your computer which is sitting to the side. This is especially true when you have a video monitor window on that computer that pulls your attention away from the teleprompter because it's brighter, larger, and syncs to the audio
    * *Mitigation:* External stereo speakers that mount to teleprompter
1. **Problem (Minor):** It's harder to know when your camera is off when it's mounted in a teleprompter. And that’s a problem if your office is a guest bedroom (or something)
    * *Mitigations*
        * Get some sort of “on air” light
        * Get a black silk hood to flip over the camera
        * Remember to turn the camera off every single time
1. **Problem (Minor):** The video quality is slightly degraded by the physics of light passing through an additional pane of glass
    * *Mitigations*
        * Adjust lighting
        * Just live with the difference (not that bad)

## External keyboard and mouse

Laptop keyboards are engineered to *fit*, not to be ergonomic. Whenever possible, consider working in a space where there's room to utilize an external keyboard. This allows you to adjust the keyboard so that you're typing in a natural way, reducing strain on your wrist and fingers.

There are a wide variety of ergonomic keyboards, and it's worth trying out a few in a retail location if possible. If this is not practical, Wirecutter has assembled [a well-researched guide on the best ergonomic keyboards](https://thewirecutter.com/reviews/comfortable-ergo-keyboard/){:target="_blank"}. 

Traditional mice can put strain on the wrist by creating movements in an unnatural position. Fortunately there are plenty of other options available such as vertical mice, trackpads, trackballs and pen tablets. Each has their own advantages. Find one that is comfortable for you and minimizes wrist movement as much as possible (vertical mice, for example, engage your shoulder and arm more than your wrist and trackballs rely on moving your fingers). You can also consider a left-handed mouse. Painless Movement has [a regularly updated list of the best ergonomic mice](https://www.painlessmovement.com/best-ergonomic-mouse-wrist-pain/){:target="_blank"} as well as comprehensive information on the various types towards the bottom of the page. It's important for remote workers to be cognizant of repetitive motion. While it may seem extreme to some, consider installing two mice and switching hands to move one's cursor. 

## Ergonomic considerations

In colocated settings, it is often possible to work with someone trained in environmental design or ergonomics in order to create a comfortable workspace. For remote employees, you can consider hiring a trained ergonomic consultant to provide professional input and recommendations on how your home office should be constructed. 

## Finding the ideal temperature

Research has [found](https://www.bbc.com/worklife/article/20160617-the-never-ending-battle-over-the-best-office-temperature){:target="_blank"} that there is no single temperature in which all people are more productive. One  issue with colocation is the divide between those who believe an office is too warm, and those who feel that it's too cold. Those new to remote work should consider testing several temperatures to see which suits them, or adjust temperature based on your work activity. 

## Busy/available indicators

While remote workers should relish the benefits of being close to friends and family while working, some may prefer a more formal approach to signaling their availability. 

For example, the [Luxafor Flag](https://luxafor.com/flag-usb-busylight-availability-indicator/){:target="_blank"} and [Luxafor Switch](https://luxafor.com/luxafor-switch-meeting-room-availability-indicator-light/){:target="_blank"} light indicators utilize a color system to alert those around you (or outside of your home office) whether or not they are free to interrupt without requesting permission. 

Alternatively, those with a nearby light fixture could install a color-changing smart bulb, utilizing their phone or computer to change the color to indicate availability. 

## GitLab Knowledge Assessment: Considerations for a productive home office or remote workspace

Anyone can test their knowledge on considerations for a productive home office or remote workspace by completing the [knowledge assessment](https://docs.google.com/forms/d/e/1FAIpQLSeuswfs_t1Dl2vbmapTLNRHa2rfi5zwUg2IQ7ZQI7hxc1gB5g/viewform){:target="_blank"}. Earn at least an 80% or higher on the assessment to receive a passing score. Once the quiz has been passed, you will receive an email acknowledging the completion from GitLab. We are in the process of designing a GitLab Remote Certification and completion of the assessment will be one requirement in obtaining the [certification](/handbook/people-group/learning-and-development/certifications/). If you have questions, reach out to our [Learning & Development team](/handbook/people-group/learning-and-development/) at `learning@gitlab.com`.

## <%= partial("company/culture/all-remote/is_this_advice_any_good_remote.erb") %>

## Contribute your lessons

GitLab believes that all-remote is the [future of work](/company/culture/all-remote/vision/), and remote companies have a shared responsibility to show the way for other organizations who are embracing it. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/){:target="_blank"} and adding a contribution to this page.

----

Return to the main [all-remote page](/company/culture/all-remote/).
