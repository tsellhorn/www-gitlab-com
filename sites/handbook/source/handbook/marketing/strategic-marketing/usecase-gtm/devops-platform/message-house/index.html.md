---
layout: markdown_page
title: "DevOps Platform Message House"
---



| Positioning Statement: | *An Open DevOps Platform to visualize and optimize workflows, collaborate across teams and projects, and deliver results faster and more securely.* |
|------------------------|-------------------------------------------------------------------------|
| **Short Description** | An Open DevOps Platform removes operational overhead, enables seamless collaboration, and allows businesses to deliver results faster, more securely, and more efficiently. |
| **Long Description** | GitLab is a complete, open DevOps Platform deployed as a single application, increasing transparency, enabling collaboration, and increasing the quality, velocity, and security of business results. GitLab eliminates the overhead and cost of integrating and managing complex toolchains, allowing you to focus on creating business value.

One underlying data store enables end-to-end transparency so every stakeholder—from portfolio managers to developers to security experts to customers—can collaborate in the development process and understand exactly what and why changes were made, by whom, and how they affected product quality, security, and compliance. One application means greater efficiency, better security, higher quality, and more value delivered for the same time and cost. |

The DevOps Platform is also a vehicle for transformation, allowing businesses to not just reduce the overhead of delivering software, but to anchor initiatives to change the way the company approaches work and collaboration.


| **Key-Values** | Value 1: | Value 2: | Value 3: |
|--------------|----------|----------|----------|
| **Promise** | **Efficiency**:<br>Focus on value—add, not infrastructure, with a single application to meet all your DevOps needs. | **Deliver Better Products Faster**:<br>Iterate faster and innovate together throughout the product lifecycle. | **Security and Compliance**:<br>Increase security and reduce compliance risk with built-in scanning and end-to-end traceability. |
| **Pain points** |  - Wasted time and resources maintaining toolchain integrations<br> - Silos of tool-specific competencies<br> - "Data gaps" in integrations resulting in incomplete context and manual workarounds<br> - Expensive to license and support | - Poor velocity and business agility<br> - Struggle to measure efficiency and understand where processes break down<br> - Difficulty moving from analysis to action<br> - correlating process changes with efficiency outcomes<br> - lack of consistent measurement across projects and teams | - Unsatisfying trade-offs<br> - Remediating vulnerabilities late in the development process is costly and time-consuming<br> - forensics and audits create an enormous burden across the entire team<br> - Difficulty scaling app sec to DevOps iterations and velocity |
| **Why GitLab** | A single application eliminates the need to maintain brittle integrations<br> A shared data store allows any user in any stage of the development process to access data from any other stage, improving efficiency via collaboration and transparency<br> Security and Compliance policies can be automated and applied consistently across projects and from end-to-end in the SDLC. | - As an end-to-end Open DevOps platform built on a single data store, GitLab provides complete visibility into all of your work, including requirements creation, planning, code changes, security and quality reviews, deplopyments, and all collaboration throughout the process. This enables out-of-the-box analytics that can be customized to your workflows, and a variety of dashboards that surface relevant information to specific roles within your organization, while allowing everyone to actively contribute with just a few clicks. | - Application security scans run seamlessly with each commit, allowing the developer to find and fix them early and within their workflow.<br> - The infrastructure required of modern apps is monitored and protected<br> - The Security Dashboard provides insights security pros need, showing remaining vulnerabilities across projects and/or groups, along with actions taken, by whom and when.<br> - All actions, from planning to code changes to approvals, are captured and correlated for easy traceability during audit events. |

### Segment messaging and positioning

Below are summaries of the value different segments will find in the DeOps Platform use case and suggestions for how to position GitLab to best expain those values.

#### SMB

All SMBs are dealing with some form of resource constraints as they grow. A DevOps platform will provide a growth path with the smallest amount of friction as they grow into the company they aspire to become.

Some SMBs will have a very clear idea of what they want from a DevOps solution, while others are just starting on their journey. For businesses in the former category, a DevOps platform provides a guide toward a productive solution that anticipates their needs as they grow, supported by evidence from successful companies of all sizes using the same solution, including some of the world's largest companies. A DevOps platform can guarantee minimal headaches from problems they have yet to encounter.

SMBs who are just starting with DevOps have likely already begun the process of integrating multiple tools, and are likely already resource constrained. These organizations are very aware of the time they waste on maintaining and integrating point solutions, and they would probably love to focus on more productive work.  They will eventually need to know that a DevOps platform can both meet their criteria and stand up to point solutions--particularly in Source Code Management and Continuous Integration. As with the other category of SMBs, customer evidence from relevant larger companies will be important third-party proof points.

#### Mid-Market

Mid-Market companies are large enough and mature enough to have experienced the same challenges as larger enterprises, and much of the messaging mentioned in the Enterprise entry will generally resonate. Efficiency is essential, freeing up resources to develop more value, more quickly. Collaboration across the organization is also critical.

The key difference between the Mid-Market and Enterprise segments is the growth imperative. Mid-Market businesses are highly oriented toward growth and innovation, as they cannot yet compete on pure scale with larger competitors, but are large enough to be outmaneuvered by nimble startups. As such, they are very interested in moving quickly, but also wary of lock-in or poor scalability that could ultimately cap their upward or outward growth. To these businesses, proof points from larger businesses--particularly those of competitors or those from similar industries--are extremely helpful. The ability to adopt a platform on the fly--one component at a time, as opportunities arise--will be well-received.

#### Enterprise

Enterprises will already be familiar with the pains of the multi-product integration "toolchain tax." Those who have been pursuing a more aggressive DevOps strategy will likely be spending a large amount of their resources building and maintaining more than one toolchain, while others may have scaled back their ambitions because of complexity and cost.

In either case, the efficiency of a single, end-to-end platform will resonate across all roles. Allocating fewer resources to maintenance and integration means less waste and more focus on productive, interesting outcomes. Additionally, a DevOps platform allows enterprises to improve the _quality_ of the work they do by increasing visibility and collaboration and encouraging a deeper engagement in all parties with the SDLC. Planners and business stakeholders can trace their requirements and suggestions all the way to their business impact after deployment, while developers and operations and security professionals can understand and contribute to the context around business discussions. Security, compliance, and audits have complete traceability back to the source of irregularities.

Single-platform visibility and collaboration dovetails nicely with transformation initiatives, as well. As one platform for end-to-end DevSecOps, GitLab can become the engine of transformation from project- to product-based thinking, waterfall- to Agile-based methodologies, or other cultural and process transformations.

### Elevator pitches per segment and persona

Below are one-sentence summaries of value you can deliver to user and buyer personas to position relevant aspects of the DevOps Platform use case's value, based on the market segment of the persona's company.

{::options parse_block_html="true" /}

<div class="panel panel-info">

**SMB**
{: .panel-heading}

<div class="panel-body">

|   User: [Delaney - the Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)	|   User: [Devon - the DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)	|   Buyer: CTO<code>&ast;</code>	|  Buyer: CIO<code>&ast;</code> 	|
|---	|---	|---	|---	|
|   Receive immediate feedback on the quality, performance, and security of code as soon as you commit. Collaborate seamlessly among teams.	|   Grow without having to build and support custom integrations.	|   Collaborate in a single system, minimize context-switching, and increase developer productivity and focus.	|   Grow without building and supporting custom integrations. Scale and manage a single system.	|
{: .custom-class #custom-id}

*<code>&ast;</code> CTO (Dev, external focus) and CIO (Ops, internal focus) personas in development and may vary per segment.*

</div>

##### **Mid-Market**
{: .panel-heading}

<div class="panel-body">

|   User: [Delaney - the Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)	|   User: [Devon - the DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)	|   Buyers: range from [Erin - the Application Development Executive](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#erin---the-application-development-executive-vp-etc) to the CTO<code>&ast;</code>	|  Buyers: range from [Kennedy - the Infrastructure Engineering Director](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#kennedy---the-infrastructure-engineering-director) to the CIO<code>&ast;</code> 	|
|---	|---	|---	|---	|
|   Receive immediate feedback on the quality, performance, and security of code as soon as you commit. Collaborate seamlessly among teams.	|   Increase reliability and eliminate ad hoc, team-based integrations.	|  Collaborate in a single system, minimize context-switching and waiting, identify and remove productivity blockers, and deliver more value faster with more productive, focused developers. 	|   Increase reliability and performance while you grow by eliminating custom integrations. Scale and manage a single system.	|
{: .custom-class #custom-id}

*<code>&ast;</code> CTO (Dev, external focus) and CIO (Ops, internal focus) personas in development and may vary per segment.*

</div>

##### **Enterprise**
{: .panel-heading}

<div class="panel-body">

|   User: [Delaney - the Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)	|   User: [Devon - the DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)	|   Buyer: [Erin - the Application Development Executive](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#erin---the-application-development-executive-vp-etc)	|  Buyer: [Kennedy - the Infrastructure Engineering Director](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#kennedy---the-infrastructure-engineering-director) 	|
|---	|---	|---	|---	|
|   Receive immediate feedback on the quality, performance, and security of code as soon as you commit. Collaborate seamlessly among teams.	|   Increase reliability and eliminate ad hoc, team-based integrations.	|   Collaborate in a single system, minimize context-switching and waiting, identify and remove productivity blockers, and deliver more value faster with more productive, focused developers.	|   Increase reliability and performance while you grow by eliminating custom integrations. Scale and manage a single system.	|
{: .custom-class #custom-id}

</div></div>

### Proof points

*(list specific analyst reports, case studies, testimonials, etc)*

- [Glympse case study](/customers/glympse/)
(~20 tools consolidated into GitLab; Glympse remediated security issues faster than any other company in their Security Auditor's experience)

> "Development can move much faster when engineers can stay on one page and click buttons to release auditable changes to production and have easy rollbacks; everything is much more streamlined. Within one sprint, just 2 weeks, Glympse was able to implement security jobs across all of their repositories using GitLab’s CI templates and their pre-existing Docker-based deployment scripts."<br>**Zaq Wiedmann**<br>Lead Software Engineer, Glympse

***

- [BI Worldwide case study](/customers/bi_worldwide/)
(BI Worldwide performed 300 SAST/Dependency scans in the first 4 days of use, helping the team identify previously unidentified vulnerabilities.)

> "One tool for SCM+CI/CD was a big initial win. Now wrapping security scans into that tool as well has already increased our visibility into security vulnerabilities. The integrated Docker registry has also been very helpful for us. Issue/Product management features let everyone operate in the same space regardless of role."<br>Adam Dehnel<br>Product architect, BI Worldwide

***

- [Gartner 2020 Market Guide for DevOps Value Stream Delivery Platforms](https://page.gitlab.com/resources-report-gartner-market-guide-vsdp.html)

***
