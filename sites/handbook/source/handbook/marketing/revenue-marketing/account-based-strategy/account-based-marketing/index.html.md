---
layout: handbook-page-toc
title: "Account Based Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Account Based Marketing
Account based marketing is a strategic approach to marketing based on account awareness in which an organization considers and communicates with individual prospect or customer accounts as markets of one. Through a close strategic alignment between sales and marketing, we focus on target accounts that fit our ICP (ideal customer profile).

## Where does account based marketing fit within the greater marketing org?
Account Based Marketing sits next to Field Marketing and is similarly aligned to Sales and Sales Development. The Demand Gen team creates marketing campaigns that support the top and middle of the funnel, while Field Marketing brings the regional knowledge and is focused on lead generation and [account centric marketing](/handbook/marketing/revenue-marketing/field-marketing/#what-does-account-centric-mean). Account centric and Account Based Marketing both allow marketers to take a targeted approach at scale in varying degrees so that we are going after the prospects and accounts _we_ want (based on our ICP and other factors) as opposed to trying to generate demand to come to us. 

## Communication
**GitLab**

If you need assistance, please open an issue in our project and/or tag us using the ~Account Based Marketing label 
- [ABM project](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing) 
- Label: `Account Based Marketing`: pulls the issue into the board and is used to put an issue on the team's radar
- Label: `ABM FYI`: used to put something on the account based marketing team's radar when they are not the DRI (example: and account centric campaign being run by field marketing)

**Slack**

- #abmteam

## Roles & Responsibilities
**Emily Luehrs**  
*Manager, Account Based Marketing*
* **Prioritization** plans prioritization of the teams work on projects, campaign and strategy
* **Development**: plan account based marketing strategy, prioritize company objectives as it aligns with ABM
* **Strategy**: plan, prioritize and manage execution of campaigns
* **Ideal Customer Profile**: acts as project manager for the development of our ICP

**Megan O'Dowd and Christina McLeod**  
*Manager, Account Based Marketing*
* **Strategy** develops and implements campaign strategy
* **Campaigns** executes on account based marketing campaigns globally

## ABM Target Account Tiers

### 1:1 Account Based Marketing (targeted) - Marketing strategy built specifically for a single targeted account
This approach combines sales intelligence and marketing muscle to build a strategy specifically for each 1:1 account that puts our Sales and Sales Development teams in front of the right people with a message that resonates. ABM tactics are personalized for each 1:1 account.

### 1:Few Account Based Marketing (scale) - Marketing strategy built for a few smaller groups of accounts with commonalities 
Accounts will be grouped based on commonalities such as industry, entry point use case, etc. and each group will receive a specific marketing strategy that is a medium lift and as personalized as we can be at scale. For example, there may be three different campaigns running within this tier that are focused on a set of accounts rather than customized to a single account.

### 1:Many Account Based Marketing (programmatic/account centric) - Marketing Strategy for a larger bucket of accounts
The ABS team is responsible for 1:1 and 1:Few marketing strategies and we collaborate with Field Marketing on the 1:Many or [account centric](/handbook/marketing/revenue-marketing/field-marketing/#what-does-account-centric-mean) approach. This marketing strategy leverages ABM tactics with less personalization so the principles can be applied to a larger number of target accounts grouped together. 

## How Accounts are added and move through ABM strategy

### Nominating an account for ABM
Accounts can be nominated by leveraging the `Account Rank` field in Salesforce. When an account is marked as [Rank 1.5](/handbook/sales/field-operations/gtm-resources/#definitions-for-enterprise-account-ranking), it is considered nominated for ABM strategy. You can nominate an account at any time, however, nominations are reviewed the last month of a quarter. Once a new quarter starts, accounts will not be added unless there is capacity, budget and/or ABM manager bandwidth. Accounts must have [specific information](/handbook/marketing/revenue-marketing/account-based-strategy/account-based-marketing/#prepping-an-account-for-account-based-marketing) up to date in Salesforce prior to being nominated.

### When does an account get added to account based marketing strategy or move tiers?
The account based marketing team will be monitoring accounts in all tiers and adjusting the level of marketing support for these accounts based on how they align to our ICP, SAL and SDR engagement (we can't build a highly personalized strategy without sales/account intelligence!), strategic priority to GitLab, and ABM Manager bandwidth (building an entire strategy for one account takes a lot of lift and time). The account team will be notified when an account is added or removed from the ABM program as well as when an account moves tiers to ensure we can strategize and build in the marketing plan appropriately. 

### When does an account get removed from account based marketing strategy?
An account can be removed from (or not added to) account based marketing if the following occur:
- [Qualification score](/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/#qualification-score) is below 60.
- The account is not owned by a SAL
- If there is no engagement/involvement from SAL and SDR (attending kick-off and check-in meetings, etc.) 
- If we don't see an increase in [metrics](/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/#abm-metrics): journey stage, velocity, engagement/lift, SAOs and/or pipeline contribution 

## Prepping an account for account based marketing
The below items need to up to date in Salesforce before an account can be added to ABM strategy. This is done by the account owner prior to nomination. If this information is not populated as best as we can with the most up to date information, the account cannot be added. 

#### Salesforce 
* [ ]  Country
* [ ]  Industry
* [ ]  Domain
* [ ]  SDR assigned 
* [ ]  Account Owner (must be a SAL)
* [ ]  Stage Technology section
* [ ]  Account hierarchy is correct and all child accounts roll up into the correct parent account

## ABM Accounts: What to expect

### Kick-off Call
If an account is added to ABM strategy, the account team will be notified via an issue and a kick-off call will be scheduled. During this kick-off call, the below items will be discussed. After the call, the ABM manager will update the issue with all of the relevant information and provide a list of what tactics are being launched, when they are being launched, and what assistance is needed. 

#### Kick-off call agenda
**ABM Manager**
- [Metrics:](/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/#abm-metrics) baseline, current and goal metrics for the account. 
- GitLab epic specific to the ABM strategy for the account. This will remain open for the duration of the campaign and will be used to track meeting notes, account updates, ongoing metrics, schedule of tactics, etc.
- Basic account campaign strategy based on standard tactics, account research, and both onsite and offsite intent data

**SAL/SDR**
- Lay of the land: different business units, what we know about the hierarchy, level of GitLab awareness   
- Common themes heard from leads and contacts
- Ideas for adjusting the campaign strategy and tactics (i.e. - use account specific verbiage for advertisements)
- Account history (former customer information, what is going on with current opp, why previous opp closed lost, etc.)
- Account tech stack/competition - if additional information outside of what is in SFDC

### Recurring Meeting/Async Communication
##### 1:1 Accounts
1:1 accounts will have a bi-weekly recurring meeting. When we can take this meeting async and update the epic, we will. The purpose of this meeting is to continue to gather account information and provide updates on how the tactics are going. 

##### 1:Few Accounts
The same items will be discussed as in a 1:1 campaign but the frequency will be once a month. Async metrics and updates will be added to the epic bi-weekly. 

## ABM Tools and Tactics

### Demandbase 
The Account Based Marketing Team owns Demandbase for GitLab.  It is a targeting and personalization platform that we use to target online ads to companies that fit our ICP and tiered account criteria.  Demandbase also has a wealth of intent data that is available to us through it's integration with Salesforce. For more information about this tool and how you can request leveraging it check out the [Demandbase handbook page](/handbook/marketing/revenue-marketing/account-based-strategy/demandbase) 

**ABM Campaign Tactics:**
- Demandbase Display ads: paid advertisements shown on websites targeting a certain account or group of accounts.
- Onsite and offsite intent data

#### Pathfactory
[Pathfactory](/handbook/marketing/marketing-operations/pathfactory/) is used to create customized content experiences and guided journeys for ABM target accounts. 

**ABM Campaign Tactics:**
- Bingeable content tracks: a collection of content compiled specifically for an account or group of accounts. 
- Customized landing pages: a microsite that can be personalized for an account that holds a collection of content specifically compiled for the account.

#### Drift
[Drift](/handbook/marketing/marketing-operations/drift/) is a chat platform used by the Sales Development (SDR) organization to engage visitors on select web pages.

**ABM Campaign Tactics:**
- Chatbot flow with verbiage and talking points personalized to the account

#### Marketo Nurture Path:
A series of emails that are sent based on a prospect's behavior, which deliver timely, targeted information that helps guide the lead through the buying process.

**ABM Campaign Tactics:**
- Account specific or topic specific nurture path 

#### Terminus Email Banner
Ad campaigns in the form of email signature banners 

**ABM Campaign Tactics:**
- Target an account or group an account with relevant messaging 

#### Webcasts and high-value offers
**ABM Campaign Tactics:**
- Webcasts can be hosted for an account (1:1) or a group of accounts (1:Few) with a topic or theme that resonates. These webcasts can also be made available on-demand  
- high-value offer event: wine tasting, cooking class, etc.  
