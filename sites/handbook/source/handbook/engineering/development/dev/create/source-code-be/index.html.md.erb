---
layout: handbook-page-toc
title: "Create:Source Code BE Team"
description: The Create:Source Code BE team is responsible for all backend aspects of the product categories that fall under the Source Code group of the Create stage.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The Create:Source Code BE team is responsible for all backend aspects of the product categories that fall under the [Source Code group][group] of the [Create stage][stage] of the [DevOps lifecycle][lifecycle].

[group]: /handbook/product/categories/#source-code-group
[stage]: /handbook/product/categories/#create-stage
[lifecycle]: /handbook/product/categories/#devops-stages

## Team Members

The following people are permanent members of the Create:Source Code BE Team:

<%= direct_team(manager_role: 'Backend Engineering Manager, Create:Source Code') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] (Create(?!:)|Create:Source Code)/, direct_manager_role: 'Backend Engineering Manager, Create:Source Code') %>

## Sisense & KPIs

<%= partial("handbook/engineering/development/dev/create/source-code-be/metrics.erb") %>

## Hiring

This chart shows the progress we're making on hiring. Check out our
[jobs page](/jobs/) for current openings.

<%= hiring_chart(department: 'Create:Source Code BE Team') %>

## Workflow

We use the standard GitLab [engineering workflow](/handbook/engineering/workflow/). To create an issue for the Create:Source Code BE team, add these labels:

- ~backend
- ~"devops::create"
- ~"Category:Source Code Management"
- ~"group::source code"

For more urgent items, feel free to use `#g_create_source_code` on Slack.

[Take a look at the features we support per category here.](/handbook/product/categories/features/#createsource-code-group)

[engineering workflow]: /handbook/engineering/workflow/
[GitLab]: https://gitlab.com/gitlab-org/gitlab
[#g_create_source_code]: https://gitlab.slack.com/archives/g_create_source-code

### Working with product

Weekly calls between the product manager and engineering managers (frontend and backend) are listed in the "Source Code Group" calendar. Everyone is welcome to join and these calls are used to discuss any roadblocks, concerns, status updates, deliverables, or other thoughts that impact the group. 

Every 2 weeks (in the middle of a release), a mid-milestone check-in occurs, to report on the current status of `~"Deliverable"`s. Monthly calls occurs under the same calendar where the entire group is encouraged to join, in order to highlight accomplishments/improvements, discuss future iterations, review retrospective concerns and action items, and any other general items that impact the group.

#### Issue refinement

To ensure we are living our iteration value consistenly, we should be intentional in asking ourselves: "is this in the smallest possible form it could be?". To that end engineers, designer, EMs, and PM should work together to find the smallest feature set that delivers value to users and can be used to elicit feedback for future iterations. Once a feasible issue plan comes together. Let's consider the following steps for best results:

1. Once we have validated the problem, product, UX, and engineering will collaborate to propose a solution and decide on what's technically feasible. The proposed solution will be shared with users to validate it solves the problem.
1. Once we have confirmed the proposed solution is viable, we will move to break it down as much as possible. When issues are ready for this stage, PM will mark issues with `workflow::refinement` label to signal next step.
1. EM will work with engineers to decide distribution of work and assign issues for breakdown.
1. Engineers or EM will evaluate the issue, work with PM, UX, and other engineering counterparts where necessary to address questions and concerns.
1. If the planned implementation of the issue can be further broken down, the engineer/EM will work with the PM to reduce scope and create new issues until this is the case (either PM or engineer/EM can create new work items).
1. When the planned implementation of the issue is in its smallest form, the engineer/EM will provide a weight. EM or PM will mark as `workflow::needs issue review`.

**Note**: if an issue receives a weight > 3 after this process, it may indicate the IC may not have a full idea of what is needed and further research is needed. 

### Convention over configuration

As stated in our direction, we must place special emphasis on our [convention over configuration](https://about.gitlab.com/direction/create/source_code_management/#critical-product-principles) principle. As the feature set within Create:Source Code grows, it may feel natural to solve problems with configuration. To ensure this is not the case, we must intentionally challenge MVC and new feature issues to check for this. Let's consider the following steps for best results:

1. Once issues have been labeled as `workflow::needs issue review` PM will share the proposal with either a peer or their manager as well as engineering (EM or IC) and product designer.

1. Peers in product and engineering who review the issue should look for opportunities to eliminate configuration where possible. If opportunities are identified, the issue is moved back to `workflow::refinement`.

1. If PM and peers are satisfied with the proposal and it follows our convention over configuration principle as much as possible, those who reviewed the issue indicate their agreement with the proposal (with either 👍 or a comment in the issue). Finally, PM or EM will label issue `workflow:: ready for development`.

### Collaborating with counterparts

You are encouraged to work as closely as needed with stable counterparts outside of the PM. We specifically include quality engineering and application security counterparts prior to a release kickoff and as-needed during code reviews or issue concerns.

Quality engineering is included in our workflow via the [Quad Planning Process](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6318).

Application Security will be involved in our workflow at the same time that [kickoff emails](#kickoff-emails) are sent to the team, so that they are able to review the upcoming milestone work, and notate any concerns or potential risks that we should be aware of.

### Triage Process

The weekly Triage Report is generated automatically by the [GitLab bot](https://gitlab.com/gitlab-bot) and this report is reviewed by the EM and Staff Engineer. Here is [an example](https://gitlab.com/gitlab-org/quality/triage-reports/-/issues/2700) of a previous report.

The Triage Report can be quite long, and it important to deal with it efficiently. An effective way to approach it is:

* Open every issue in a separate browser tab and use "edit issue" to mark then as checked once review, then close the tab.
* Verify if the issue belongs to ~"group::source code" and change group label if needed. The [Features by Group](https://about.gitlab.com/handbook/product/categories/features/#createsource-code-group) page is a good starting point for this assessment.
* Apply ~frontend if it is a frontend issue.
* Perform a brief search to assess if is a duplicate, close with a ~Duplicate label if this is the case.
* Is it a ~"support request" ? Does it ~"needs investigation" ? Apply labels if so.
* Apply ~regression label if it is one, consider bumping severity numbers if recent.
* Apply ~"severity::4", ~"priority::4", %Backlog if a smaller issue with no significant impact.
* If an uncontroversial problem with a clear solution, consider applying ~"Accepting merge requests"
* If also an easier issue which might interest a newer community contributor, consider applying ~"low hanging fruit", ~"good for new contributors".
* Apply ~"priority::3" ~"severity::3" if a bug with a workaround.
* Anything causing data loss, severe performance impact or security apply a ~"severity::1"  and ~"priority::1" or ~"priority::2" and assign to a team member.
* Unassign yourself from the Triage Report

### Engineering cycle

The engineering cycle is centered around the [GitLab Release Date of the 22nd of the month](https://about.gitlab.com/handbook/engineering/releases/#timelines). This is the only fixed date in the month, and the table below indicates how the other dates can be determined in a given month.

#### Candidate issues

Urgent issues are tentatively assigned to a release to ensure other teams have visibility.

At this point the issues are _Candidate_ issues, and the milestone does not confirm that they will be definitely scheduled. Issues move from _Candidate_ status to confirmed during the [Issue selection](#issue-selection) process.

##### How do I know an issue is scheduled?

- The [Issue Selection](#issue-selection) process has concluded.
- The milestone is indicated.
- The issue is assigned to an engineer.
- The ~"workflow::ready for development" label is added.

#### Key dates

| Activity | Date | Non-working day? |
| Release Start | 18th of the month | Next working day |
| Feature Freeze | 17th of next month | Previous working day |
| Release        | 22nd of next month | Even on non-working day |
| [Planning Issue Review](#planning-issue-review) | Thursday before [Capacity Planning](#capacity-planning) |
| [Slippage reporting](#slippage-reporting) | 14th of the next month | Previous working day |
| [Needs Weight Issue](#needs-weight-issue)  | 15th of the next month | Previous working day |
| [Capacity Planning](#capacity-planning) | 15th of the next month | Previous working day |
| [Issue Selection](#issue-selection) | 16th of the next month | Previous working day |
| [Issue Assignments](#issue-assignments) | 17th of next month | Previous working day |

#### Activities

```mermaid
graph TD;
    Features-->PI[Planning Issue];
    Bugs-->PI[Planning Issue];
    Security-->PI[Planning Issue];
    Performance-->PI[Planning Issue];
    InfraDev-->PI[Planning Issue];
    AL[App Limits]-->PI[Planning Issue];
    PI[Planning Issue]-->NWI[Needs Weight Issue];
    CP[Capacity Planning]-->Slippage;
    Slippage-->IS[Issue Selection];
    NWI[Needs Weight Issue]-->IS[Issue Selection];
    IS[Issue Selection]-->IA[Issue Assignments];
```

#### Needs Weight issue

> If you haven't read the code, you haven't investigated deeply enough
>
> -- <cite>Nick Thomas</cite>

A list of candidate issues for the release will be distributed to team members in a Needs Weight issue [_example_](https://gitlab.com/gitlab-org/create-stage/-/issues/12797) to "weight" or estimate the capacity needed to complete the work. The EM attempts to limit each team member to 4-5 issues, and the estimates should be timeboxed to \~4 hours. If an issue can't be weighted in this window, it requires a research spike.

##### Weighting issues

1. See if there is already a discussed backend solution/plan or none yet.
1. If the discussed backend solution/plan isn't that clear, clarify it.
1. If there's no solution/plan yet, devise one. Doesn't need to be a detailed solution/plan. Feel free to ask other people to pick their brains.
1. If there's a need to collaborate with stable counterpart to devise a solution/plan, add a comment and tag relevant counterparts.
1. Give issue a weight if there's none yet or update if the existing weight isn't appropriate anymore. Optionally leave a comment describing why a certain weight is given.
1. It's strongly encouraged to spend no more than 1 hour per issue. Give it your best guess and move on if you run out of time.
1. Label the issue as ~"workflow::ready for development" if you feel we can make progress on it in the next milestone.

Add a comment and ping the EM if you would like to be assigned to work on this issue in the upcoming release.

##### Weight categories

We use a lightweight system of issue weighting to help with capacity planning.
These weights help us ensure that the amount of scheduled work in a cycle is
reasonable, both for the team as a whole and for each individual. The "weight
budget" for a given cycle is determined based on the team's recent output, as
well as the upcoming availability of each engineer.

The weights we use are:

| Weight | Description  |
| --- | --- |
| 1: Trivial | The problem is very well understood, no extra investigation is required, the exact solution is already known and just needs to be implemented, no surprises are expected, and no coordination with other teams or people is required.<br><br>Examples are documentation updates, simple regressions, and other bugs that have already been investigated and discussed and can be fixed with a few lines of code, or technical debt that we know exactly how to address, but just haven't found time for yet. |
| 2: Small | The problem is well understood and a solution is outlined, but a little bit of extra investigation will probably still be required to realize the solution. Few surprises are expected, if any, and no coordination with other teams or people is required.<br><br>Examples are simple features, like a new API endpoint to expose existing data or functionality, or regular bugs or performance issues where some investigation has already taken place. |
| 3: Medium | Features that are well understood and relatively straightforward. A solution will be outlined, and most edge cases will be considered, but some extra investigation will be required to realize the solution. Some surprises are expected, and coordination with other teams or people may be required.<br><br>Bugs that are relatively poorly understood and may not yet have a suggested solution. Significant investigation will definitely be required, but the expectation is that once the problem is found, a solution should be relatively straightforward.<br><br>Examples are regular features, potentially with a backend and frontend component, or most bugs or performance issues. |
| 4: Large | Features that are well understood, but known to be hard. A solution will be outlined, and major edge cases will be considered, but extra investigation will definitely be required to realize the solution. Many surprises are expected, and coordination with other teams or people is likely required.<br><br>Bugs that are very poorly understood, and will not have a suggested solution. Significant investigation will be required, and once the problem is found, a solution may not be straightforward.<br><br>Examples are large features with a backend and frontend component, or bugs or performance issues that have seen some initial investigation but have not yet been reproduced or otherwise "figured out". |
| 5: Unknown | A feature that is weight 5 will not be scheduled and instead should be broken down or a spike scheduled |

A weight of 5 generally indicates the problem is not clear or a solution should be instead converted to an Epic with sub-issues.

###### If the issue should be broken down

If the problem is well-defined but too large (weight 5 or greater), either:

- Promote the issue to an Epic and break the work into sub-issues. Weight the individual issues if possible.
- Ping @sean_carroll and @sarahwaldner and outline the reason the issue needed to be promoted to an Epic.

###### If the issue SSOT is not clear

- Don't assign a weight, instead add a comment indicating what needs clarification and ping @sean_carroll and @sarahwaldner.

##### If the issue needs a spike

- Don't assign a weight, instead add a comment about the need for a spike (and possibly what would be investigated) and ping @sean_carroll / @sarahwaldner.
- Spikes are scheduled with a weight of 2.

Security issues are typically weighted one level higher than they would normally
appear from the table above. This is to account for the extra rigor of the
[security release process](https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/developer.md).
In particular, the fix usually needs more-careful consideration, and must also
be backported across several releases.

#### Planning issue review

The Source Code stable counterparts (BE, FE, PM, UX) meet and propose issues to be worked on in the upcoming release. Using the [Mural](https://www.mural.co/) visual collaboration tool, candidate issues are voted on by the group.

#### Capacity planning

Capacity planning is a collaborative effort involving all Source Code team members and stable counterparts from Frontend, UX and Product. An initial list of issues is tracked in the Source Code Group Planning issue [example](https://gitlab.com/gitlab-org/create-stage/-/issues/12783) for each month.

##### Team Availability

Approximately 5-10 business days before the start of a new release, the EM will begin determining how "available" the team will be. Some of the things that will be taken into account when determining availability are:

* Upcoming training
* Upcoming time off / holidays
* Upcoming on-call slots
* Potential time spent on another teams deliverables

Availability is a percentage calculated by _(work days available / work days in release) * 100_.

All individual contributors start with a "weight budget" of 10, meaning they are capable (based on historical data) of completing a maximum number of issues worth 10 weight points total (IE: 2 issues which are weighted at 5 and 5, or 10 issues weighted at 1 each, etc.) Then, based on their availability percentage, weight budgets are reduced individually. For example, if you are 80% available, your weight budget becomes 8.

Product will prioritize issues based on the teams total weight budget. Our [planning rotation](#planning-rotation) will help assign weights to issues that product intends on prioritizing, to help gauge the amount of work prioritized versus the amount we can handle prior to a kickoff.

##### Source Code issue pipeline

The Source Code issue pipeline is broad, and the PM and EM work together throughout the planning process and the final list is chosen during the Issue Selection meeting. The issue pipeline includes:

- Features
- Bugs
- Security issues
- [Infradev board issues](https://gitlab.com/gitlab-org/gitlab/-/boards/706619?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Asource%20code&label_name[]=infradev)
- [Performance board issues](https://gitlab.com/gitlab-org/gitlab/-/boards/706619?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Asource%20code&label_name[]=performance-refinement)
- [Application limit board issues](https://gitlab.com/gitlab-org/gitlab/-/boards/706619?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Asource%20code&label_name[]=Application%20Limits)

#### Issue Selection

On or around the 16th, the PM and EM meet once more to finalize the list of issues in the release. The issue board for that release is then updated, and any issues with an candidate milestone that are not selected will be moved to Backlog or added as a candidate for a future release.

Issues scheduled for the release are then marked ~"workflow::ready for development".

#### Issue assignments

Once availability has been determined, weights have been assigned, and the PM/EM finalize a list of prioritized issues for the upcoming release, kickoff emails will be sent. The intent of this email is to notify you of the work we intend to assign for the upcoming release. This email will be sent before the release begins. The kickoff email will include:

* Your availability, weight budget, and how it was calculated
* A list of the issues you will most probably be assigned as an individual
* A reasoning behind why you have been assigned more than your weight budget, if applicable
* A list of the issues the team is working on that are deemed "note-worthy," in case you'd like to offer help on those issues as time allows

Emails get sent to each individual contributor on the team, as well as the Application Security counterpart, in order to give a heads-up about the upcoming issues in the milestone and what the assignments will be.

##### Kickoff emails

The Kick-off emails are sent manually by the EM, and use this template:

```
Hi _team member_,

For 14.1, you are available 90% of the time, so your weight budget is 9.

This is due to: Family and Friends Day 2021-06-25 and OOO 2021-06-24.

These are the issues you're being assigned, please speak up if they
are unreasonable (blocked, too large, etc):

- Mirrors won't download new LFS files
https://gitlab.com/gitlab-org/gitlab/-/issues/331364

_list each issue here_

Here is the build board for the group:

https://gitlab.com/groups/gitlab-org/-/boards/363876?label_name%5B%5D=group::source%20code&label_name%5B%5D=Deliverable&label_name%5B%5D=backend&milestone_title=14.1

Many thanks for all of your efforts,

Sean

```

#### Follow-up issues

You will begin to collect follow-up issues when you've worked on something in a release but have tasks leftover, such as technical debt, feature flag rollouts or removals, or non-blocking work for the issue. For these, you can address them in at least 2 ways:
* Add an appropriate future milestone to the follow-up issue(s) with a weight and good description on the importance of working this issue
* Add the issue(s) to the relevant [planning issue](https://gitlab.com/gitlab-org/create-stage/-/issues?scope=all&utf8=%E2%9C%93&state=opened&search=source+code+group+planning)
 
You should generally take on follow-up work that is part of our [definition of done](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#definition-of-done), preferably in the same milestone as the original work, or the one immediately following. If this represents a substantial amount of work, bring it to your manager's attention, as it may affect scheduling decisions.

If there are many follow-up issues, consider creating an epic.

#### Backend and Frontend issues

Many issues require work on both the backend and frontend, but the weight of that work may not be the same. Since an issue can only have a single weight set on it, we use scoped labels instead when this is the case: `~backend-weight::<number>` and `~frontend-weight::<number>`.

### Workflow labels

<%= partial("handbook/engineering/development/dev/create/workflow_labels.erb", locals: { group_label: 'group::source code' }) %>

### Async standup

<%= partial("handbook/engineering/development/dev/create/async_standup.erb") %>

### Retrospectives

We have 1 regularly scheduled "Per Milestone" retrospective, and can have ad-hoc "Per Project" retrospectives.

#### Per Milestone

<%= partial("handbook/engineering/development/dev/create/retrospectives.erb", locals: { group: "Source Code", group_slug: 'source-code' }) %>

#### Per Project

If a particular issue, feature, or other sort of project turns into a particularly useful learning experience, we may hold a synchronous or asynchronous retrospective to learn from it. If you feel like something you're working on deserves a retrospective:
1. [Create an issue](https://gitlab.com/gl-retrospectives/create-stage/source-code/issues) explaining why you want to have a retrospective and indicate whether this should be synchronous or asynchronous
2. Include your EM and anyone else who should be involved (PM, counterparts, etc)
3. Coordinate a synchronous meeting if applicable

All feedback from the retrospective should ultimately end up in the issue for reference purposes.

### Deep Dives

<%= partial("handbook/engineering/development/dev/create/deep_dives.erb") %>

### Career development

<%= partial("handbook/engineering/development/dev/create/career_development.erb", locals: { group: "Source Code" }) %>

### Performance Monitoring

The Create:Source Code BE team is responsible for keeping some API endpoints and
controller actions performant (e.g. below our target speed index).

Here are some Kibana visualizations that give a quick overview on how they perform:

- [Create::Source Code: Controller Actions](https://log.gprd.gitlab.net/app/kibana#/visualize/edit/32698f60-b145-11ea-bfe2-25f984e253f8?_g=(filters%3A!()%2CrefreshInterval%3A(pause%3A!t%2Cvalue%3A0)%2Ctime%3A(from%3Anow-7d%2Cto%3Anow)))
- [Create::Source Code: Endpoints](https://log.gprd.gitlab.net/app/kibana#/visualize/edit/104d4bf0-a0d9-11ea-8cfd-8dcd98a55a1d?_g=(filters%3A!()%2CrefreshInterval%3A(pause%3A!t%2Cvalue%3A0)%2Ctime%3A(from%3Anow-7d%2Cto%3Anow)))

These tables are filtered by the endpoints and controller actions that the group
handles and sorted by P90 (slowest first) for the last 7 days by default.
