---
layout: handbook-page-toc
title: "Delivery Team"
description: "The Delivery Team enables GitLab Engineering to deliver features in a safe, scalable and efficient fashion to both GitLab.com and self-managed customers."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Common Links

| **Workflow** | [Team workflow](/handbook/engineering/infrastructure/team/delivery/#how-we-work) |
| **GitLab.com** | `@gitlab-org/delivery` |
| **Issue Trackers** | [**Delivery**][issue tracker]|
| **Slack Channels** | [#g_delivery] / `@delivery-team`
| **Delivery Handbook** | [Team training][team training]|
| Release Tools Project | [Release tools](/handbook/engineering/infrastructure/release-tools) |
| Release Manager Runbooks | [release/docs/runbooks](https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/README.md) |

## Mission

The Delivery Team enables GitLab Engineering to deliver features in a
**safe**, **scalable** and **efficient** fashion to both GitLab.com and self-managed customers.
The team ensures that GitLab's monthly, security, and patch releases are deployed to GitLab.com and
publicly released in a timely fashion.

## Vision

By its own nature, the Delivery team is a backstage, non-user feature facing team whose product
and output has a direct impact on Infrastructure's primary goals of **availability**, **reliability**,
**performance**, and **scalability** of all of GitLab's user-facing services as well as self-managed
customers. The team creates the workflows, frameworks, architecture and automation for Engineering teams
to see their work reach production effectively and efficiently.

The Delivery team is focused on our [CI/CD blueprint](https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/library/ci-cd/index.md)
by driving the necessary changes in our software development processes and workflows, as well as
infrastructure changes, to embrace the benefits of CI/CD.

### Short-term

* Automate release generation, removing most of manual work
* Automate deployment process, managing and limiting impact on production
* Simplify security releases
* Streamline the process of limiting feature impact in production environments
* Enable feature testing at production-environment scale
* Create detailed architecture blueprints and design for CD on GitLab.com
* Develop and track KPIs to measure the team's impact on GitLab product delivery

### Mid-term

* Drive the implementation of infrastructure changes to prepare GitLab.com for CD
* Eliminate the need for feature-freeze blackouts during the development cycle
* Shorten build times to allow for faster release times

### Long-term

* Drive necessary changes that will lead to Kuberentes-based infrastructure on GitLab.com
* Fully automated releases for self-managed users

## Strategy

The Delivery team significantly contributes to [the Infrastructure department direction for FY22](/handbook/engineering/infrastructure/index.html#fy22-direction) in the following ways:

1. [Performance](/handbook/engineering/infrastructure/index.html#1-best-of-class-performance)
  * By the end of Q2FY22: [Complete the migration of all stateless services](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/112) to the Kubernetes platform.
  * By the end of Q4FY22: Define, plan and execute on migration of stateful services to the Kubernetes platform.
  * By the end of Q2FY22: [Define deployment SLO](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1362), and evaluate its impact on Infrastructure performance indicators.
1. [Reliability](/handbook/engineering/infrastructure/index.html#3-make-reliability-the-platform-strength)
  * By the end of Q3FY22: Ensure predictable and reliable rollback of stateless services.
1. [External contributions](/handbook/engineering/infrastructure/index.html#5-generate-contributions-external-to-the-department)
  * By the end of Q4FY22: Convert at least four release workloads to GitLab features, thus enabling external contributions to the features.

## Top-level Responsibilities

The team regularly works on the following tasks, in the order of priority:

1. Ensuring continous delivery of GitLab application software to GitLab SaaS.(e.g. [GitLab SaaS auto-deploy](https://gitlab.com/groups/gitlab-org/release/-/epics/13))
1. Coordination and preparation of GitLab releases for self-managed users for the monthly, patch and security releases.
1. Participating in incident resolution and acting on corrective actions for SaaS and self-managed software delivery.
1. Increasing velocity of both SaaS and self-managed software delivery through foundational project work (e.g. [Running GitLab SaaS on Kubernetes](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/112)).
1. Improving the robustness of SaaS software delivery by creating and improving tooling (e.g. [Deployment rollback](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/282)).
1. Minimizing the use of custom tooling by building or enhancing features within GitLab (e.g. [Create a Changelog feature](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/351)).
1. Support other teams' needs related to software delivery on GitLab SaaS (e.g. [New Container Registry deployment](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/412)).

## Team

Each member of the Delivery team is part of this vision:

* Each team member is able to work on all team projects
* The team is able to reach a conclusion independently all the time, consensus most of the time
* Career development paths are clear
* Team creates a database of knowledge through documentation, training sessions and outreach

### Team Members

The following people are members of the Delivery Team:

<%= direct_team(manager_role: 'Engineering Manager, Delivery')%>

## Team counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Delivery/, direct_manager_role: 'Engineering Manager, Delivery') %>

## Performance indicators

Delivery team contributes to [Engineering function performance indicators] through [Infrastructure department performance indicators].
The team's main performance indicator is [**M**ean **T**ime **T**o **P**roduction][MTTP] (MTTP), which serves to show how quickly a change introduced through a Merge Request
is reaching production environment (GitLab.com).
At the moment of writing, the target for this PI is defined in this [key result][KI lower MTTP] epic.

MTTP is further broken down into charts and tables at the [Delivery Team Performance Indicators Sisense dashboard][Delivery Sisense PIs].

## How we work

### Project Management

The Delivery team work is tracked through number of epics, issues, and issue boards.

Epics and issue boards are complementary to each other, and we always strive to have a 1-1 mapping between a working epic and an issue board.
Epics describe the work and allows for general discussions, while the issue board is there to describe order of progress in any given epic.

### Epics

Two tracking epics related to the team mission are:

1. [GitLab.com on Kubernetes](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/112)
1. [Release Velocity](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/170)

Any working epic that the team creates should be directly added as a child to one of these two top level tracking epics.

Working epic should always have:

1. [A Problem Statement](https://lamport.azurewebsites.net/pubs/state-the-problem.pdf).
1. Link to an issue board used to organize work described in the epic.
1. Clear indication of the epic status in the epic description.
    * ✅ - completed
    * ⏹ - not started
    * ⏳ - in progress
1. ![Team label](img/label-team.png)
1. Label used as part of the project scope (eg. `kubernetes`, `security-release`).
1. [Directly responsible individuals][DRI] responsible for the project completion.
1. Start and due date.
1. Defined exit criteria.
1. Status YYYY-MM-DD to indicate what is being worked on, why, and planned next steps.

In cases where the work is tracked in a project in a different group outside of our canonical project location, we will create two epics for the same topic and state in the epic description which one is the working epic.  It is expected that the Status of the Epic is updated on the DRI on a weekly basis.

### Issue Boards

Each working epic should be accompanied by an issue board. Issue boards should be tailored to the specific project needs, but at minimum it should contain the [workflow labels](#workflow-labels) shown on the workflow diagram.

### Labels

The canonical issue tracker for the Delivery team is at [gl-infra/delivery][issue tracker]. Issues are automatically labeled if no labels are applied using the [triage ops] project.
The default labels defined in the [labeling library](https://gitlab.com/gitlab-com/gl-infra/triage-ops/-/blob/master/lib/delivery/default_labeling.rb).

By default, an issue needs to have a:

1. Workflow Label - Default: `workflow-infra::Triage`
1. Priority Label - Default: `Delivery::P4`
1. Other Label - project or team management related label.

#### Workflow

The Delivery team leverages scoped `workflow-infra` labels to track different stages of work.

On the [Planning board] problems are discussed and scoped so that we have enough information to prioritise and implement the solution.

The typical workflow is described below:

```mermaid
sequenceDiagram
  participant triage as workflow-infra::Triage
  participant proposal as workflow-infra::Proposal
  participant ready as workflow-infra::Ready
   triage ->> proposal: 1
Note right of triage: Problem has been identified and a  possible solution has been proposed.
   proposal ->> ready: 2
Note right of proposal: Proposal has been discussed and scoped. Issue is ready for prioritization.
```

Not every issue will be prioritised for building as soon as it is ready. Instead we manage a [Build board] with all `workflow-infra::In Progress`, and `workflow-infra::Ready` issues focused on the team's current goals.

The standard progression of workflow is described below:

```mermaid
sequenceDiagram
  participant ready as workflow-infra::Ready
  participant progress as workflow-infra::In Progress
  participant done as workflow-infra::Done
        ready ->> progress: 1
Note right of ready: Issue is assigned and<br/> work has started.
    progress ->> done: MR is merged and deployed to production
Note right of progress: Issue is updated with<br/>rollout details,<br/> workflow-infra::Done<br/> label is applied,<br/> issue can be closed.
```

There are three other workflow labels of importance omitted from the diagram above:

1. `workflow-infra::Cancelled`:
  - Work in the issue is being abandoned due to external factors or decision to not resolve the issue. After applying this label, issue will be closed.
1. `workflow-infra::Stalled`
  - Work is not abandoned but other work has higher priority. After applying this label, team Engineering Manager is mentioned in the issue to either change the priority or find more help.
1. `workflow-infra::Blocked`
  - Work is blocked due external dependencies or other external factors. After applying this label, issue will be regularly triaged by the team until the label can be removed.

Label `workflow-infra::Done` is applied to signify completion of work, but its sole purpose is to ensure that issues are closed when the work is completed, ensuring issue hygiene.

#### Priority Labels

The Delivery team uses priority labels to indicate order under which work is next to be picked up. Meaning attached to priorities can be seen below:

| Priority level  | Definition |
| --------------- | ---------- |
| Delivery::P1 | Issue is blocking other team-members, or blocking other work. Needs to be addressed immediately, even if it means postponing current work. |
| Delivery::P2 | Issue has a large impact, contributes towards current OKRs or will create additional work. Work should start as soon as possible after completing ongoing task. |
| Delivery::P3 | Issue should be completed once other urgent work is done. |
| Delivery::P4 | **Default priority**. A nice-to-have improvement, non-blocking technical debt, or a discussion issue. Issue might be completed in future or work completely abandoned. |

The team uses priority labels differently to the [general issue triage priority definition](/handbook/engineering/quality/issue-triage/#priority) in order to avoid ambiguity that comes with difference in timelines between Stage teams and Infrastructure teams. We have different timelines (release brings different expectations for Delivery), different DRI's (no PM for Delivery), and different importance (Blocked release means that no one can ship anything).

#### Other Labels

Some of the labels related to the team management are defined as:

1. `onboarding` - issues are related to granting access to team resources.
1. `team-tasks` - issues related to general team topics.
1. `Discussion` - meta issues that are likely to be promoted to a working epic or generate separate implementation issues.
1. `Announcements` - issues used to announce important changes to wider audience.

Project labels are defined as needed, but they are required unless the issue describes a team management task.

### Choosing something to work on

The Delivery team generally has working epics assigned to specific owners who are responsible for bringing tasks into the [Planning board] and [Build board] to keep the project on track. However, anyone is welcome to pick up any tasks from the Build board regardless of which project it belongs to.
If you want to work on something outside of your current project, feel free to contribute to any of the mid to lower priority labeled issues.

## Project demos

As part of the project, we might decide to organize project demo's. The decision on creating a demo depends on the expected longevity of the project, but also on the complexity of it.

The purpose of the demo is to ensure that everyone who participates in the project has a way of sharing their findings and challenges they might be encountering outside of the regular async workflow. The demo's do not have presentations attached to it, and they require no prior preparation.
The demoer shouldn't feel like they have to excuse themselves for being unprepared, and expect that their explanation without faults. In fact, if what is being demoed is showing off no weaknesses, we might have not cut scope in time.

It is encouraged to show and discuss:

1. The imperfectness of the specific code implementation.
1. How broken the used tool might be.
1. How an estabilished process breaks down.
1. How challenging a problem being resolved might be.

### Team training

Every Delivery team member is responsible for sharing skills either through creating a training session for the rest of the team, or through paired work.
See the page on [team training] for details.

## History

The Delivery team officially [came into existence on 2018-10-23](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/15348/). This was the culmination of a larger alignment that was happening throughout that year, exposed by the need to streamline releases for self-managed users and creating a better experience for GitLab.com users.

All throughout GitLab's existence, Release Management had been a monthly rotating role served by developers. The idea behind it was to keep developers close to the whole lifecycle of the software they create, and ensure that they automate their work. This worked well until the number of application changes, and developer tasks grew too large for anyone to handle as a secondary task. The event that indicated the need for a change was a near miss event near the end of 2017, when the first Release Candidate was deployed to GitLab.com just 2 days before the 22nd. That whole month was riddled with challenges, from release managers struggling to deliver their day to day development tasks and RM tasks, to multiple unsuccessful deployments to GitLab.com. Most importantly, this was a first indication that the company was growing and that the processes that worked previously, might need to change to accommodate the larger growth that was planned.

After some internal discussions, we entered 2018 with an attempt to [work on process improvements](https://gitlab.com/gitlab-org/release/tasks/-/issues/39), rather than changing everything in one go. We went from a monthly rotation to two month [release manager rotation](/community/release-managers/), started [noting down spent time](https://gitlab.com/gitlab-org/release/tasks/-/issues/1). Over the next several months we'd seen general stabilization of the process but it became apparent that spending 4 engineers time in Release Manager rotation was not getting us anywhere closer to improving the deployment process for GitLab.com, and with each developer we hired the task list grew bigger.

The initial discussion on [what is in front of us to achieve Continuous Delivery](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1) on GitLab.com exposed a clear need for a team focused on this specific task.

With the team created, we set out to work on the [release we designed](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/16028), [change the way we deploy to GitLab.com](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/17842/diffs), [merge GitLab Rails codebases](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/19037/diffs) and many more other tasks.

After a successful [team onsite (aka Fast boot)](https://gitlab.com/groups/gitlab-org/release/-/epics/17) where we executed on our tasks while being in the same room together for the first time, [we announced the first step towards Continous Delivery on GitLab.com](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/345). This was a very large change that changed the deployment frequency from deploying from the default branch once per month (for the total of 4-6 deploys to include bug fixes), to taking commits from the default branch once per week.

The team focus then shifted to getting deployment time measured in hours, and migration of GitLab.com to Kubernetes.

Prior to 2020, the team impact overview was created in Slack, and in the years that followed the overview was logged in issues:

1. [Year overview for 2020](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1446)

[issue tracker]: https://gitlab.com/gitlab-com/gl-infra/delivery
[team training]: /handbook/engineering/infrastructure/team/delivery/training/
[#g_delivery]: https://gitlab.slack.com/archives/g_delivery
[#production]: https://gitlab.slack.com/archives/production
[#infrastructure-lounge]: https://gitlab.slack.com/archives/infrastructure-lounge
[#incident-management]: https://gitlab.slack.com/archives/incident-management
[Engineering function performance indicators]: /handbook/engineering/performance-indicators/
[Infrastructure department performance indicators]: /handbook/engineering/infrastructure/performance-indicators/
[MTTP]: /handbook/engineering/infrastructure/performance-indicators/#mean-time-to-production-mttp
[KI lower MTTP]: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/107
[Delivery Sisense PIs]: https://app.periscopedata.com/app/gitlab/573702/WIP:-Delivery-team-PIs
[triage ops]: https://gitlab.com/gitlab-com/gl-infra/triage-ops
[DRI]: /handbook/people-group/directly-responsible-individuals/
[Planning board]: https://gitlab.com/gitlab-com/gl-infra/delivery/-/boards/2048133?&label_name[]=Delivery%20team%3A%3APlanning&label_name[]=team%3A%3ADelivery
[Build board]: https://gitlab.com/gitlab-com/gl-infra/delivery/-/boards/1918862?&label_name[]=Delivery%20team%3A%3ABuild&label_name[]=team%3A%3ADelivery
